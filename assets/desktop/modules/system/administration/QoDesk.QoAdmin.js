/*
 * Coral Desktop 0.1
 * Copyright(c) Coral Media Solutions, LLC.
 * https://www.coralmediasolutions.com
 */

QoDesk.QoAdmin = Ext.extend(Ext.app.Module, {
   id: 'system-administration',
   type: 'system/administration',
       requires: [
          'app/system/administration/lib/TooltipEditor',
          'app/system/administration/lib/Nav',
          'app/system/administration/lib/ActiveColumn',
          'app/system/administration/lib/ColumnNodeUI',
          'app/system/administration/lib/SearchField',
          'app/system/administration/lib/members/MembersTooltipEditor',
          'app/system/administration/lib/members/Members',
       ],

   actions: null,
   defaults: { winHeight: 600, winWidth: 800 },
   tabPanel: null,
   win: null,

   createWindow : function(){
      let desktop = this.app.getDesktop();
      this.win = desktop.getWindow(this.id);

      let h = parseInt(desktop.getWinHeight() * 0.9);
      let w = parseInt(desktop.getWinWidth() * 0.7);
      if(h > this.defaults.winHeight){ h = this.defaults.winHeight; }
      if(w > this.defaults.winWidth){ w = this.defaults.winWidth; }

      let winWidth = w;
      let winHeight = h;

      if(this.win){
         //this.win.setSize(w, h);
      }else{
         this.tabPanel = new Ext.TabPanel({
            activeTab:0
            , border: false
            , items: new QoDesk.QoAdmin.Nav(this)
         });

         this.win = desktop.createWindow({
            animCollapse: false
            , cls: 'qo-win'
            , constrainHeader: true
            , id: this.id
            , height: winHeight
            , iconCls: 'qo-admin-icon'
            , items: [
               this.tabPanel
            ]
            , layout: 'fit'
            , shim: false
            , taskbuttonTooltip: '<b>Control Center</b><br />Allows you to administer your desktop'
            , title: 'Control Center'
            , width: winWidth
         });
      }
        
      this.win.show();
   }
    
   , openTab : function(tab){
      if(tab){
         this.tabPanel.add(tab);
      }
      this.tabPanel.setActiveTab(tab);
   }
    
   , viewGroups : function(){
      let tab = this.tabPanel.getItem('qo-admin-groups');
      if(!tab){
         tab = new QoDesk.QoAdmin.Groups({ ownerModule: this });
         this.openTab(tab);
      }else{
         this.tabPanel.setActiveTab(tab);
      }
   }
    
   , viewMembers : function(){
      let tab = this.tabPanel.getItem('qo-admin-members');
      if(!tab){
         tab = new QoDesk.QoAdmin.Members({ ownerModule: this });
         this.openTab(tab);
      }else{
         this.tabPanel.setActiveTab(tab);
      }
   }

   , viewPrivileges : function(){
      let tab = this.tabPanel.getItem('qo-admin-privileges');
      if(!tab){
         tab = new QoDesk.QoAdmin.Privileges({ ownerModule: this });
         this.openTab(tab);
      }else{
         this.tabPanel.setActiveTab(tab);
      }
   }
    
   , viewSignups : function(){
      let tab = this.tabPanel.getItem('qo-admin-signups');
      if(!tab){
         tab = new QoDesk.QoAdmin.Signups(this);
         this.openTab(tab);
      }else{
         this.tabPanel.setActiveTab(tab);
      }
   }

   , showMask : function(msg){
      this.win.body.mask(msg+'...', 'x-mask-loading');
   }

   , hideMask : function(){
      this.win.body.unmask();
   }
});