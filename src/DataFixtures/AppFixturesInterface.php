<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;

interface AppFixturesInterface extends ORMFixtureInterface
{
    const YAML_FILE_PATH = __DIR__ . '/../../config/fixtures';

    public function loadYamlData($className);
}
