<?php

namespace App\DataFixtures\ORM;

use App\DataFixtures\AppFixturesInterface;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class UserFixtures extends Fixture implements AppFixturesInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function loadYamlData($className)
    {
        return Yaml::parse(
            file_get_contents(
                AppFixturesInterface::YAML_FILE_PATH .
                DIRECTORY_SEPARATOR . 'users.yaml'
            )
        )['fixtures'][$className];
    }

    public function load(ObjectManager $manager)
    {
        $data = $this->loadYamlData(User::class);

        foreach ($data as $record) {
            $entity = new User();
            $entity->setEmail($record['email']);
            $entity->setFirstName($record['firstName']);
            $entity->setLastName($record['lastName']);
            $entity->setRoles($record['roles']);
            $entity->setPassword(
                $this->passwordEncoder->encodePassword(
                    $entity, $record['password']
                )
            );

            $manager->persist($entity);

            $this->addReference(sha1($entity->getEmail()), $entity);
        }

        $manager->flush();
    }
}
