<?php

namespace App\DataFixtures\ORM;

use App\DataFixtures\AppFixturesInterface;
use App\Entity\Wallpaper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

class WallpaperFixtures extends Fixture implements AppFixturesInterface
{
    public function loadYamlData($className)
    {
        return Yaml::parse(
            file_get_contents(
                AppFixturesInterface::YAML_FILE_PATH .
                DIRECTORY_SEPARATOR . 'wallpapers.yaml'
            )
        )['fixtures'][$className];
    }

    public function load(ObjectManager $manager)
    {
        $data = $this->loadYamlData(Wallpaper::class);

        foreach ($data as $record) {
            $entity = new Wallpaper();
            $entity->setName($record['name'])
                ->setFile($record['file'])
                ->setActive($record['active'])
            ;

            $manager->persist($entity);

            $this->addReference(sha1($entity->getName()), $entity);
        }

        $manager->flush();
    }
}
