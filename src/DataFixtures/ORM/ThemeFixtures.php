<?php

namespace App\DataFixtures\ORM;

use App\DataFixtures\AppFixturesInterface;
use App\Entity\Theme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

class ThemeFixtures extends Fixture implements AppFixturesInterface
{
    public function loadYamlData($className)
    {
        return Yaml::parse(
            file_get_contents(
                AppFixturesInterface::YAML_FILE_PATH .
                DIRECTORY_SEPARATOR . 'themes.yaml'
            )
        )['fixtures'][$className];
    }

    public function load(ObjectManager $manager)
    {
        $data = $this->loadYamlData(Theme::class);

        foreach ($data as $record) {
            $entity = new Theme();
            $entity->setName($record['name'])
                ->setAuthor($record['author'])
                ->setVersion($record['version'])
                ->setThumbnail($record['thumbnail'])
                ->setActive($record['active'])
            ;

            $manager->persist($entity);

            $this->addReference(sha1($entity->getName()), $entity);
        }

        $manager->flush();
    }
}
