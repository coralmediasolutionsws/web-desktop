<?php

namespace App\Controller\Desktop;

use App\Service\Desktop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

abstract class AbstractDesktopController extends AbstractController
{
    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            'app.desktop' => Desktop::class,
        ]);
    }
}
