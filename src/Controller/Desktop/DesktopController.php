<?php

namespace App\Controller\Desktop;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DesktopController extends AbstractDesktopController
{
    /**
     * @Route("/", name="desktop_index")
     */
    public function index()
    {
        return $this->render('desktop/desktop/index.html.twig', [
            'modules' => $this->get('app.desktop')->hydrateModulesData(),
            'desktopConfig' => $this->get('app.desktop')->hydrateDesktopConfig()
        ]);
    }
}
