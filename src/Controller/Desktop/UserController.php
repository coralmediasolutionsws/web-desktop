<?php

namespace App\Controller\Desktop;

use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractDesktopController
{
    /**
     * @Route("/desktop/user/get_all", name="desktop_user_get_all")
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request)
    {
        $qb = $this->getDoctrine()->getRepository(User::class)
            ->createQueryBuilder('u');
        $adapter = new DoctrineORMAdapter($qb);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setMaxPerPage((int) $request->get('limit'));

        $pagerfanta->setCurrentPage(
            $pagerfanta->getPageNumberForItemAtPosition(
                (int) $request->get('start') + 1
            )
        );

        $responseData = [];

        /**
         * @var User $user
         */
        foreach ($pagerfanta->getCurrentPageResults() as $user) {
            $item = [];
            $item['id'] = $user->getId();
            $item['firstName'] = $user->getFirstName();
            $item['lastName'] = $user->getLastName();
            $item['email'] = $user->getEmail();
            $responseData[] = $item;
        }

        return new JsonResponse([
            'success' => true,
            'total' => $pagerfanta->getNbResults(),
            'data' => $responseData
        ]);
    }
}
